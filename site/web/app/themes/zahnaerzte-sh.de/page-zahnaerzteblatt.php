<?php
/**
 * Template Name: Zahnärzteblatt
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>

  <table>
    <tbody>

      <?php while( have_rows('ausgabe') ): the_row();

        // vars
        $jahr = get_sub_field('jahr');
        $monat = get_sub_field('monat');


        echo '
          <tr>
            <td>'.$jahr.'</td>
        '?>

        <?php while( have_rows('monat') ): the_row();

          // vars
          $titel = get_sub_field('titel');
          $pdf = get_sub_field('pdf');

          echo '
              <td><a href="'.$pdf.'">'.$titel.'</a></td>
          '?>

        <?php endwhile; ?>

        <?php echo '
          </tr>
        '?>

      <?php endwhile; ?>

    </tbody>
  </table>


<?php endwhile; ?>
