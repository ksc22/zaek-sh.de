<?php use Roots\Sage\Titles; ?>

<div class="page-header">
  <h1><?= Titles\title(); ?></h1>
</div>
<p><small>
  <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
Sie sind hier:
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
  </div>
</small></p>
<hr />
