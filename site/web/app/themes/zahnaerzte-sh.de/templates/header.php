<header>

  <div class="head">
    <div class="textWrap">
      <div class="name"><a  href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a></div>
      <div class="description"><a  href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('description'); ?></a></div>
    </div>

    <div class="logoWrap">
      <div><img src="<?= get_template_directory_uri() . '/dist/images/logo.svg'; ?>" height="90px"></div>
    </div>
  </div>

</header>
