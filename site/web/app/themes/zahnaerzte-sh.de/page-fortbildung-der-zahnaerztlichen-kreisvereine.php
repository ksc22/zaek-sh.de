<?php
/**
 * Template Name: Fortbildung der zahnärztlichen Kreisvereine
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>

  <?php if( have_rows('dithmarschen') ): ?>

    <h5>Kreisvereinigung der Zahnärzte Dithmarschens</h5>

    <?php while( have_rows('dithmarschen') ): the_row();

      // vars
      $termin = get_sub_field('termin');
      $dozent = get_sub_field('dozent');
      $thema = get_sub_field('thema');
      $ort = get_sub_field('ort');
    ?>

    <p>

      <b><?php echo $termin." Uhr</b><br>"; ?>
      <?php echo $dozent.":<br>"; ?>
      <?php echo $thema."<br>"; ?>
      Ort: <?php echo $ort."<br>"; ?>

    </p>

    <?php endwhile; ?>

  <?php endif; ?>

  <?php if( have_rows('flensburg') ): ?>

    <h5>Zahnärzteverein Flensburg e.V.</h5>

    <?php while( have_rows('flensburg') ): the_row();

      // vars
      $termin = get_sub_field('termin');
      $dozent = get_sub_field('dozent');
      $thema = get_sub_field('thema');
      $ort = get_sub_field('ort');
    ?>

    <p>

      <b><?php echo $termin." Uhr</b><br>"; ?>
      <?php echo $dozent.":<br>"; ?>
      <?php echo $thema."<br>"; ?>
      Ort: <?php echo $ort."<br>"; ?>

    </p>

    <?php endwhile; ?>

  <?php endif; ?>

<?php endwhile; ?>
