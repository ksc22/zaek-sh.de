<?php
/**
 * Template Name: Vorstand
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>

  <?php if( have_rows('vorstand') ): ?>

      <?php while( have_rows('vorstand') ): the_row();

      // vars
      $name = get_sub_field('name');
      $ort = get_sub_field('ort');
      $funktion = get_sub_field('funktion');
      $email = get_sub_field('email');
      $foto = get_sub_field('foto');
      ?>

      <p>

      <?php if( $foto ): ?>
        <a href="<?php echo $foto; ?>">
          <img src="<?php echo $foto; ?>" style="width: 25%; height: auto;">
        </a>
      <?php else: ?>
        <?php echo "<i>(kein Foto)</i>"; ?>
      <?php endif; ?>

      <br>

      <?php if( $email ): ?>
        <a href="mailto:<?php echo $email; ?>">
      <?php endif; ?>

      <?php echo $name.", ".$ort; ?>

      <?php if( $email ): ?>
        </a>
      <?php endif; ?>

      <?php echo "<br>".$funktion; ?>

      </p>

    <?php endwhile; ?>

  <?php endif; ?>

<?php endwhile; ?>
