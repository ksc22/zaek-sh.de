<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>

  <?php if( have_rows('startitems') ): ?>

	<ul>

	<?php while( have_rows('startitems') ): the_row();

		// vars
		$titel = get_sub_field('titel');
		$beschreibung = get_sub_field('beschreibung');
		$link = get_sub_field('link');

		?>

    <p>

      <?php if( $link ): ?>
        <a href="<?php echo $link; ?>">
      <?php endif; ?>

        <?php echo "<strong>".$titel."</strong>"; ?>

      <?php if( $link ): ?>
        </a>
      <?php endif; ?>

      <?php echo "<br>".$beschreibung; ?>

    </p>

	<?php endwhile; ?>

	</ul>

<?php endif; ?>

<?php endwhile; ?>
