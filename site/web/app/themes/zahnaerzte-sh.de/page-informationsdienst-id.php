<?php
/**
 * Template Name: Informationsdienst-ID
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>

  <table>
    <tbody>

      <?php while( have_rows('ids') ): the_row();

        // vars
        $uberschrift = get_sub_field('uberschrift');
        $pdf = get_sub_field('pdf');

        echo '
          <tr>
            <td><a href="'.$pdf.'">'.$uberschrift.'</a></td>
          </tr>
        '?>

      <?php endwhile; ?>

    </tbody>
  </table>

<?php endwhile; ?>
