<?php
/**
 * default search form
 */
?>
<form role="search" method="get" id="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="search">
      <span class="fa fa-search"></span>
      <input type="search" placeholder="Suchtext" name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>" />


    </div>
</form>
