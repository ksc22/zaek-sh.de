
<?php while (have_posts()) : the_post(); ?>
<?php get_template_part('templates/content', 'page'); ?>

  <!--Content-->
  <div class="container">

      <?php if( have_rows('kasten') ) {

          $cards = get_field('kasten');
          $cardCount = count($cards);
          $perRow = 3;
          $numRows = $cardCount/$perRow;

          for($row=0; $row<$numRows; $row++ ) {

          echo '<div class="row">';

          for($inRow=0; $inRow<$perRow; $inRow++ ) {

            the_row();

         		// vars
         		$text = get_sub_field('text');

            echo '
              <div class="col-lg-4 d-flex flex-column justify-content-strech">
                  <!--Card-->
                  <div class="card card-private">
                      <!--Card content-->
                      <div class="card-block">
                          <!--Text-->

                          <span class="card-text">'.$text.'</span>

                      </div>
                      <!--/.Card content-->
                  </div>
                  <!--/.Card-->
              </div>
              <!--columnn-->
            ';

          }

          echo '</div>';

        }

      }?>

    </div>
    <!--/.Content-->

<?php endwhile; ?>
