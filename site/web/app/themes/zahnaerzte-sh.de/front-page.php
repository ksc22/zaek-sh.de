
<?php while (have_posts()) : the_post(); ?>
<?php get_template_part('templates/content', 'page'); ?>

  <!--Content-->
  <div class="container">

      <?php if( have_rows('startitems') ) {

          $cards = get_field('startitems');
          $cardCount = count($cards);
          $perRow = 3;
          $numRows = $cardCount/$perRow;

          for($row=0; $row<$numRows; $row++ ) {

          echo '<div class="row">';

          for($inRow=0; $inRow<$perRow; $inRow++ ) {

            the_row();

         		// vars
         		$titel = get_sub_field('titel');
         		$beschreibung = get_sub_field('beschreibung');
         		$link = get_sub_field('link');
            $exlink = get_sub_field('exlink');
            $beschriftung = get_sub_field('beschriftung');

            // external link? take ist otherwise take the internal link
            if($exlink != '') {
              $link = $exlink;
            }

            echo '
              <div class="col-lg-4 d-flex flex-column justify-content-strech">
                  <!--Card-->
                  <div class="card card-public">
                      <!--Card content-->
                      <div class="card-block">
                          <!--Text-->
                          <p class="card-text">'.$beschreibung.'</p>
                          <a href="'.$link.'" class="btn btn-info">'.$beschriftung.'</a>
                      </div>
                      <!--/.Card content-->
                  </div>
                  <!--/.Card-->
              </div>
              <!--columnn-->
            ';

          }

          echo '</div>';

        }

      }?>

    </div>
    <!--/.Content-->

<?php endwhile; ?>
