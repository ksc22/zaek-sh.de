<?php
/**
 * Template Name: Pressemeldungen
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>

  <table>
    <tbody>

      <?php while( have_rows('pressemeldungen') ): the_row();

        // vars
        $datum = get_sub_field('datum');
        $meldung = get_sub_field('meldung');
        $pdf = get_sub_field('pdf');

      echo '
        <tr>
          <td><a href="'.$pdf.'">'.$meldung.'</a></td>
          <td>'.$datum.'</td>
        </tr>
      '?>

      <?php endwhile; ?>

    </tbody>
  </table>


<?php endwhile; ?>
