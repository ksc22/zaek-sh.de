# Translation of Plugins - Safe Redirect Manager - Stable (latest release) in German
# This file is distributed under the same license as the Plugins - Safe Redirect Manager - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2022-05-10 17:32:08+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/4.0.0-alpha.1\n"
"Language: de\n"
"Project-Id-Version: Plugins - Safe Redirect Manager - Stable (latest release)\n"

#: inc/functions.php:119
msgid "Gone"
msgstr "Vergangen"

#: inc/classes/class-srm-post-type.php:585
msgid "Optionally leave notes on this redirect e.g. why was it created."
msgstr "Hinterlasse optional Hinweise zu dieser Weiterleitung, z. B. warum sie erstellt wurde."

#: inc/classes/class-srm-post-type.php:583
msgid "Notes:"
msgstr "Hinweise:"

#: inc/classes/class-srm-post-type.php:481
msgctxt "redirect rule"
msgid "Create Redirect Rule"
msgstr "Weiterleitungsregel erstellen"

#: inc/classes/class-srm-post-type.php:366
msgid "Order"
msgstr "Reihenfolge"

#: inc/classes/class-srm-post-type.php:305
msgid "Redirect rule scheduled for: %1$s."
msgstr "Weiterleitungsregel eingerichtet für: %1$s."

#: inc/functions.php:118
msgid "Not Found"
msgstr "Nicht gefunden"

#: inc/functions.php:117
msgid "Forbidden"
msgstr "Unzulässig"

#: inc/functions.php:116
msgid "Temporary Redirect"
msgstr "Temporäre Weiterleitung"

#: inc/functions.php:115
msgid "See Other"
msgstr "Siehe Andere"

#: inc/functions.php:114
msgid "Found"
msgstr "Gefunden"

#: inc/functions.php:113
msgid "Moved Permanently"
msgstr "Dauerhaft verschoben"

#. Author URI of the plugin
msgid "https://10up.com"
msgstr "https://10up.com"

#. Author of the plugin
msgid "10up"
msgstr "10up"

#. Plugin URI of the plugin
msgid "https://wordpress.org/plugins/safe-redirect-manager"
msgstr "https://de.wordpress.org/plugins/safe-redirect-manager"

#. Description of the plugin
msgid "Easily and safely manage HTTP redirects."
msgstr "HTTP Weiterleitungen einfach und sicher verwalten."

#: inc/classes/class-srm-post-type.php:579
msgid "If you don't know what this is, leave it as is."
msgstr "Wenn du nicht weißt was das ist, lass es wie es ist."

#: inc/classes/class-srm-post-type.php:573
msgid "* HTTP Status Code:"
msgstr "* HTTP-Status-Code:"

#: inc/classes/class-srm-post-type.php:570
msgid "This can be a URL or a path relative to the root of your website (not your WordPress installation). Ending with a (*) wildcard character will append the request match to the redirect."
msgstr "Dies kann eine URL sein, oder eine relative Pfadangabe zu deiner Website (nicht deiner WordPress Installation). Ein Wildcard Zeichen (*) am Ende wird den gefundenen Teil des Requests ans Ende des Redirects anhängen."

#: inc/classes/class-srm-post-type.php:567
msgid "* Redirect To:"
msgstr "* Weiterleiten zu:"

#: inc/classes/class-srm-post-type.php:564
msgid "This path should be relative to the root of this WordPress installation (or the sub-site, if you are running a multi-site). Appending a (*) wildcard character will match all requests with the base. Warning: Enabling regular expressions will disable wildcards and completely change the way the * symbol is interpreted."
msgstr "Dieser Pfad sollte relativ zum Stammverzeichnis deiner WordPress-Installation sein (bzw. deiner Unter-Website, falls du eine Multisite betreibst). Ein Wildcard-Zeichen (*) am Ende wird alle Anfragen mit dieser Basis abfangen. Vorsicht: Das Aktivieren von regulären Ausdrücken wird Wildcards deaktivieren und das *-Symbol völlig anders interpretieren."

#: inc/classes/class-srm-post-type.php:562
msgid "Enable Regular Expressions (advanced)"
msgstr "Reguläre Ausdrücke aktivieren (fortgeschritten)"

#: inc/classes/class-srm-post-type.php:559
msgid "* Redirect From:"
msgstr "* Weiterleiten von:"

#: inc/classes/class-srm-post-type.php:534
msgid "Redirect Settings"
msgstr "Weiterleitungseinstellungen"

#: inc/classes/class-srm-post-type.php:489
msgid "No redirect rules found in trash."
msgstr "Keine Weiterleitungsregeln im Papierkorb gefunden."

#: inc/classes/class-srm-post-type.php:488
msgid "No redirect rules found."
msgstr "Keine Weiterleitungsregeln gefunden."

#: inc/classes/class-srm-post-type.php:487
msgid "Search Redirects"
msgstr "Weiterleitungen suchen"

#: inc/classes/class-srm-post-type.php:486
msgid "View Redirect Rule"
msgstr "Weiterleitungsregeln ansehen"

#: inc/classes/class-srm-post-type.php:484
msgid "New Redirect Rule"
msgstr "Neue Weiterleitungsregel"

#: inc/classes/class-srm-post-type.php:483
msgid "Edit Redirect Rule"
msgstr "Weiterleitungsregel bearbeiten"

#. Plugin Name of the plugin
#: inc/classes/class-srm-post-type.php:482
#: inc/classes/class-srm-post-type.php:485
#: inc/classes/class-srm-post-type.php:491
msgid "Safe Redirect Manager"
msgstr "Safe Redirect Manager"

#: inc/classes/class-srm-post-type.php:480
msgctxt "post type singular name"
msgid "Redirect"
msgstr "Weiterleitung"

#: inc/classes/class-srm-post-type.php:479
msgctxt "post type general name"
msgid "Safe Redirect Manager"
msgstr "Safe Redirect Manager"

#: inc/classes/class-srm-post-type.php:373
msgid "Date"
msgstr "Datum"

#: inc/classes/class-srm-post-type.php:369
msgid "Redirect From"
msgstr "Weiterleiten von"

#: inc/classes/class-srm-post-type.php:365
msgid "HTTP Status Code"
msgstr "HTTP Status Code"

#: inc/classes/class-srm-post-type.php:364
msgid "Redirect To"
msgstr "Weiterleiten zu"

#: inc/classes/class-srm-post-type.php:310
msgid "Redirect rule draft updated."
msgstr "Weiterleitungsregel-Entwurf aktualisiert."

#. translators: Publish box date format, see http://php.net/date
#: inc/classes/class-srm-post-type.php:307
msgid "M j, Y @ G:i"
msgstr "j. M. Y \\u\\m H:i"

#: inc/classes/class-srm-post-type.php:303
msgid "Redirect rule submitted."
msgstr "Weiterleitungsregel eingereicht."

#: inc/classes/class-srm-post-type.php:302
msgid "Redirect rule saved."
msgstr "Weiterleitungsregel gespeichert."

#: inc/classes/class-srm-post-type.php:301
msgid "Redirect rule published."
msgstr "Weiterleitungsregel veröffentlicht."

#. translators: %s: date and time of the revision
#: inc/classes/class-srm-post-type.php:300
msgid "Redirect rule restored to revision from %s"
msgstr "Weiterleitungsregel auf Revision %s wiederhergestellt"

#: inc/classes/class-srm-post-type.php:297
msgid "Custom field deleted."
msgstr "Benutzerdefiniertes Feld gelöscht."

#: inc/classes/class-srm-post-type.php:296
msgid "Custom field updated."
msgstr "Benutzerdefiniertes Feld aktualisiert."

#: inc/classes/class-srm-post-type.php:295
#: inc/classes/class-srm-post-type.php:298
msgid "Redirect rule updated."
msgstr "Weiterleitungsregel aktualisiert."

#: inc/classes/class-srm-post-type.php:244
msgid "Safe Redirect Manager Error: You have reached the maximum allowable number of redirects"
msgstr "Safe Redirect Manager Fehler: Du hast die maximal zugelassene Anzahl an Weiterleitungen erreicht."

#: inc/classes/class-srm-post-type.php:230
msgid "Safe Redirect Manager Warning: Possible redirect loops and/or chains have been created."
msgstr "Safe Redirect Manager Warnung: Mögliche Weiterleitungsschleifen und/oder Ketten wurden erstellt."

#: inc/functions.php:240
msgid "An error occurred creating the redirect."
msgstr "Ein Fehler ist beim Erstellen der Weiterleitung aufgetreten."

#: inc/functions.php:226
msgid "Redirect already exists for %s"
msgstr "Weiterleitung für %s existiert bereits"

#: inc/functions.php:221
msgid "Invalid status code."
msgstr "Ungültiger Statuscode."

#: inc/functions.php:217
msgid "Redirect from and/or redirect to arguments are invalid."
msgstr "Weiterleitung von und/oder Weiterleitung zu Parametern sind ungültig."